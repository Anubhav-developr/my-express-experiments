const express = require('express')
const path = require('path')
const hbs = require('hbs')
const app = express();
port = 5502
const viewPath = path.join(__dirname, './templates/views')
const partialsPath = path.join(__dirname, './templates/partials')
hbs.registerPartials(partialsPath);
app.set('view engine', 'hbs')
app.set('views', viewPath)
app.use(express.static(path.join(__dirname, '../public')))
// app.use(express.static(path.join(__filename, '../public/about.html')))
// app.use(express.static(path.join(__filename, '../public/help.html')))
app.use(express.static(path.join(__dirname, './assets')))

app.get('', (req, res) => {
    res.render('index', {
        name: 'anubhav',
        game: 'coding'
    })
})

app.get('/help', (req, res) => {
    res.render('help', {

        name: 'hello child'
    })
})

app.get('/faculty',(req,res)=>{
   if(!req.query.id){
   return res.send('please provide an id.')
   }
   res.send({
       id:req.query.id,
       name:'arun awashti',
       phone:'123',
       age:'12'
   })
})


app.get('/name/:user_name', (req, res) => {
    res.status(200);
    res.set('Content-type', 'text/html');
    res.send('<html><body>' +
        '<h1>Hello ' + req.params.user_name + '</h1>' +
        '</body></html>'
    );

})

app.get('/about', (req, res) => {
    res.render('about', {
        tech: 'nodejs',
        project: 'weather app',

    })
})

app.get('/weather', (req, res) => {
    // res.send('<h1>weather route</h1>')
    res.send({
        location: 'philadelphia',
        weather: 'shy'
    })
})
app.get('/help/*',(req,res)=>{
    res.render('Harticle',{
        helpRouteErr : 'Help Article NOT FOUND'
    })
})

app.get('*', (req, res) => {
    res.render(
        'error',{
            genErr : '404 Error',
           }
    )
})

// app.get('',(req,res)=>{
//     // res.send('hello express!!')
//     res.send(__dirname + ' '+ __filename)

// })




app.listen(port, () => {
    console.log('hello world')
})

